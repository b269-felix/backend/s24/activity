// Initializing variable
const firstNumber = 5;
// Exponential 
const cube = 5 ** 3;
console.log(`The cube of ${firstNumber} is ${cube}`);

// Initializing array variable and properties
let address = [258, "Washington Ave NW", "California", 90011];
let [postal, streetAddress, country, countryCode] = address;


console.log(`I live at ${postal} ${streetAddress}, ${country} ${countryCode}`);

// Initializing Object
let animal = {
	name: "Lolong",
	weight: 1075,
	height: 20,
	width: 3
};

// // Print out address in cosole using Template literals and Destructuring object properties 
console.log(`${animal.name} was a saltwater crocodile. He weighed at ${animal.weight} kgs with a measurement of ${animal.height} ft ${animal.width} in.`)

// Initializing variable and using forEach function to print out properties in indiviual lines
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) =>{
	console.log(number)
});
// Calculating the sum of numbers array using reduce method 
let reduceNumber = numbers.reduce((accumulator,curValue) => {
	return accumulator + curValue
	
}, 0);
console.log(reduceNumber);

// initializing dog class with 3 objects
class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

};
// Insantiating properties to dog class
let myDog = new dog("Bambam", "1.5 years old", "American Bully");
console.log(myDog);




